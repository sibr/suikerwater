var gulp = require('gulp');
var sass = require('gulp-sass');
var refresh = require('gulp-refresh');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('sass:development', function() {
    return gulp.src('public/scss/**/*.scss')
                                .pipe(sourcemaps.init())
                                .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
                                .pipe(sourcemaps.write())
                                .pipe(gulp.dest('public/css'))
                                .pipe(refresh())
});

gulp.task('sass:watch', function() {
    refresh.listen();
    gulp.watch('public/scss/**/*.scss', ['sass:development']);
});

gulp.task('sass:release', function() {
    return gulp.src('public/scss/**/*.scss')
                                .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
                                .pipe(gulp.dest('public/css'))
});

gulp.task('minify', function() {
        return gulp.src('public/img/*')
            .pipe(imagemin({
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
    }))
    .pipe(gulp.dest('public/img/min'));
});

gulp.task('dist', ['sass:release', 'minify']);
