# README #


## Samplework

Reproduce a sample page for siroop: [computer-tablets]

## Tasks

Ignore the navigation and the footer of the site.
Use following technologies to complete the tasks. 
Focus on something important.

Environment:

* use git 
* node.js 
* npm

HTML

* plain html or some templating system 
* keep the html semantic 
* preprocessor of your choice

CSS:

* focus on css scalability, reusability
* preprocessor of your choice (scss, stylus)
* use adaptive or responsive design
* pick 1 responsive image technik

JS:

* use taskrunner - gulp

## Additional Task if you have time left

* use server
* create the site navigation with your own design

[siroop]: https://siroop.ch
[computer-tablets]: https://siroop.ch/computer-office/computer-tablets

## Installation


```
#!bash

./setup
```

## Run Server


```
#!bash

node app.js
```

Browser-Adress: http://localhost:3000/computer-elektronik/computer-tablets
