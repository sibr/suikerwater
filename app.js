var express = require('express');
var nunjucks  = require('nunjucks');
var app = express();

app.use('/public', express.static(__dirname + '/public'));

nunjucks.configure('views', {
  autoescape: true,
  express   : app
});

app.get('/computer-elektronik/computer-tablets', function(req, res) {
  res.render('computer-tablets.html');
});

app.listen(3000, function () {
  console.log('App listening on port 3000!');
});

